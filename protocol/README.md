# Protokoll #

Inhaltsverzeichnis

- [Vorlesungswoche 1](./woche1.md)
- [Vorlesungswoche 2](./woche2.md)
- [Vorlesungswoche 3](./woche3.md)
- [Vorlesungswoche 4](./woche4.md) !!! Diese Woche sind wir im **Hörsaal 8** !!!
- [Vorlesungswoche 5](./woche5.md)
- [Vorlesungswoche 6](./woche6.md)
- [Vorlesungswoche 7](./woche7.md)
- [Vorlesungswoche 8](./woche8.md) !!! Diese Woche fällt wegen _Dies academicus_ aus (siehe Moodle) !!!
- [Vorlesungswoche 9](./woche9.md)
- [Vorlesungswoche 10](./woche10.md) <- Onlinemodus
- [Vorlesungswoche 11](./woche11.md) <- Onlinemodus
- [Vorlesungswoche 12](./woche12.md)
- [Vorlesungswoche 13](./woche13.md) ???
- [Vorlesungswoche 14](./woche14.md) ???
