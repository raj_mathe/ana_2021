# Vorlesungswoche 6 (15.–21. November 2021) #

## Agenda ##

- [ ] Lösungen zu Aufgaben in ÜB3 besprochen
- [ ] Aspekte von ÜB5 diskutiert
  - Struktur eines Arguments mit der Basisdefinition von Grenzwert

    ```
    Sei ε > 0.
    Setze n(ε) ∈ ℕ mit ...
    Dann gilt für alle n ≥ n(ε), dass
        |x_n – x| ≤ ...
            < ... [ Argument nutzt n≥n(ε) sowie die Forderungen auf n(ε) aus ]
            < ε
    ```

## Nächste Woche ##

- ÜB4 Lösungen
- ÜB6 diskutieren

### TODOs (Studierende) ###

- ÜB5 vor 10:30 (VL) od. 12:00 (Briefkasten) am Dienstag abgeben.
- Ans Vorrechnen von Aufgaben aus ÜB4 denken.
