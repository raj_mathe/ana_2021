# Vorlesungswoche 12 (10.–16. Januar 2022) #

(Wieder in Präsenz.)

## Agenda ##

- [x] Orga
    - übrige Wochen
    - Anmerkung zu Lösung von ÜB9
        - A2 (b)+(c) in Notizen
        - für A4 kommt man auch ohne ZWS aus
- [x] ÜB10: —/gleichmäßige/Lipschitz-Stetigkeit
- [x] ÜB11 (A4 wurde aktualisiert)

## Nächste Woche ##

- ÜB11 + 12

### TODOs (Studierende) ###

- ÜB11 fertig schreiben und abgeben.
- aktuellsten VL-Stoff erlernen, vertiefen, verinnerlichen.
  Man sollte u. a. fließend die versch. Stetigkeitsbegriffe bedienen können.
