# Vorlesungswoche 8 (29. November – 5. Dezember 2021) --> 7.12. #

**ACHTUNG:** Diese Woche fällt am Do die Übung aus.
Stattdessen wird ein Ersatztermin am 7.12. organisiert.

## Ablauf ##

- [x] ÜB5 vorrechnen - A1, A3, A4.
- [x] ÜB7 besprechen - A2 a), A3, A4 b).
- Weitere Fragen: ÜB6 A3.

## ~~Nächste Woche~~ (--> diesen Donnerstag) ##

- ÜB6 vorrechnen.
- Aspekte von ÜB8 diskutieren.

### TODOs (Studierende) ###

- ~~ÜB7 zu Ende schrieben und vor Frist (heute, 7.12.) abgeben.~~
- VL-Inhalte bes. Kapiteln 5 + 6 durchlesen und Stoff erlernen.
