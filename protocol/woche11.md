# Vorlesungswoche 11 (3.–9. Januar 2022) #

(online --- siehe Moodle)

## Agenda ##

- [x] Orga:
    - Abgabe nächste Woche
    - Infos zur Probeklausur
        - ✅ Aussagekraft über Schwierigkeitsgrad?
        - ❌ Aussagekraft über Zeit/schreibaufwand?
        - 〰️ Aussagekraft über Themenabdeckung?
        - ❌ Aussagekraft über Gewichtung?
    - Präsenz/online Modus nächste Woche?
    - Takt für restliche Wochen: {n-1, n+1} -> {n, n+1}
- [ ] ~~Fragen zu ÜB7 + ÜB8?~~
- [x] ÜB9
- [ ] ~~ÜB10~~

## Nächste Woche ##

- Probeklausurblatt,
- Modus für nächste Woche wird noch (vom Prof) angekündigt.

### TODOs (Studierende) ###

- weiter an ÜB10 arbeiten und (online!) abgeben.
- aktuellsten VL-Stoff durchlesen und verinnerlichen.
