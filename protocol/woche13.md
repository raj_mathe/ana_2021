# Vorlesungswoche 13 ??? (17.–23. Januar 2022) #

(Vertretung)

## Agenda ##

- [ ] ÜB11
- [ ] ÜB12

## Nächste Woche ##

- ÜB12
- Vorklausuraufgaben

### TODOs (Studierende) ###

- ÜB12 fertig schreiben und abgeben.
- Klausurvorbereitung:
  - Probeklausur / Aufgabensammlung versuchen;
  - Zusammenfassung der Kapiteln erstellen.
