# Vorlesungswoche 7 (22.–28. November 2021) #

## Agenda ##

- [x] Orga
  - [x] Abstimmung der Studierenden über Präsenz v. Digital ---> überwiegende Mehrheit für Präsenzbetrieb.
  - [x] Abgaben ab Woche 8 wieder dienstags (siehe Moodle!).
- [x] ÜB4 - A1, A2 (b)+(c), A4.
- [x] ÜB6 - Aspekte von A4 diskutiert (liminf, limsup). Ähnliche Aufgabe besprochen.

## Nächste Woche ##

- ÜB5 vorrechnen.
- Aspekte von ÜB7 diskutieren.

### TODOs (Studierende) ###

- ÜB6 zu Ende schrieben und vor Frist abgeben.
- VL-Inhalte bes. Kapiteln 2; 4; 5 durchlesen und Stoff erlernen.

### Zusatz: ###

Um „fließender“ mit sup/inf umzugehen, empfehle ich, folgende Aussagen zu beweisen:

1. Sei `(X, <)` eine dichte^ totale Ordnungsrelation. Dann für alle `a,b ∈ X` mit `a < b` gelten:

  - `sup (-∞, b) = sup (a, b) = sup [a, b) = sup (-∞, b] = sup (a, b] = sup [a, b] = b`;
  - `inf (a, +∞) = inf (a, b) = inf (a, b] = inf [a, +∞) = inf [a, b) = inf [a, b] = a`.

  (So etwas macht man ein Mal im Leben!!)

2. Sei `(X, <)` eine totale Ordnungsrelation und sei `M ⊆ X`. Dann gelten
  - `min M` existiert ⟺ `inf M` existiert und `inf M ∈ M`.
    Wenn das Minimum existiert, dann stimmen Min und Inf überein.
  - `max M` existiert ⟺ `sup M` existiert und `sup M ∈ M`.
    Wenn das Maximum existiert, dann stimmen Max und Sup überein.

3. Sei `(X, <)` eine totale Ordnungsrelation und sei `M ⊆ X` dicht in `X`.^^ Dann gelten:
  - `sup I = sup (I ∩ M)` für alle nicht leere Intervalle `I ⊆ X`
  - `inf I = inf (I ∩ M)` für alle nicht leere Intervalle `I ⊆ X`

4. Sei `(X, <)` eine totale Ordnungsrelation und sei `M ⊆ X` dicht in `X`. Seien `A ⊆ M` und `m ∈ M`. Dann gelten:
  - `A` hat Supremum `m` berechnet innerhalb `(M, <)` ⟺ `A` hat Supremum `m` berechnet innerhalb `(X, <)`.
  - `A` hat Infimum `m` berechnet innerhalb `(M, <)` ⟺ `A` hat Infimum `m` berechnet innerhalb `(X, <)`.

5. Finde ein Gegenbeispiel zu den Aussagen in 4, wenn die Dichtheitsannahme wegfällt.


^ Dass eine Ordnungsrelation, `(X, <)` **dicht** ist, bedeutet:
```
∀x,y ∈ X: (x < y ⟹ ∃z ∈ X: x < z < y).
```

^^ Dass eine Teilmenge, `M ⊆ X`, **dicht in** `(X, <)` ist, bedeutet:
```
∀x,y ∈ X: (x < y ⟹ ∃z ∈ M: x < z < y).
```
