# Vorlesungswoche 5 (8.–14. November 2021) #

Diese Woche sind wir wieder im **Felix Klein Saal**.

Bitte daran denken, dass Studierende ihre Lösungswege zu ÜB2 präsentieren können.
</br>
Wer seine/ihre Aufgabe besonders gut gelöst hat, kann sich gerne dazu melden 🙂
</br>
Mein Lösungsweg zu A3 steht in den Handnotizen, und ggf. kann ich dies gerne dazu präsentieren.

## Agenda ##

- [x] Orga
  - [x] Entscheidung von Studierenden über Takt
    --> Abstand = 2
  - [x] Gibt es Musterlösungen oder erwünschte Lösungswege?
- [x] Übungsblatt 2
  - [x] A3
- ~~[ ] Übungsblatt 3~~
- [x] Hinweise und Fragen zu Übungsblatt 4
  - [x] Begriffe
  - [x] äquivalente Definitionen:
    in (ggf partieller) Ordnungsrelation,
    in (totaler) Ordnungsrelation,
    in einem Körper.
  - [x] Beispiele mit Randfällen (leere Menge und ganze Menge)
  - [x] Beweis von sup c·M = c·sup M
    - [x] mit rein ordnungstheoretischem Ansatz
    - [x] mit ε-Ansatz

## Nächste Woche ##

- Besprechung von ÜB3 + Hinweise und Fragen zu Übungsblatt 5

### TODOs (Studierende) ###

- Stoff aus Kapiteln 1–3 verinnerlichen
- am ÜB4 weiter arbeiten
- an das Vorrechnen für ÜB3 denken
