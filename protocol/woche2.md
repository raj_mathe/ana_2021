# Vorlesungswoche 2 (18.–24. Oktober 2021) #

## Agenda ##

- [x] Orga
    - [ ] Notenvergabe (4 x 4 pro Blatt)
    - [x] Korrekturdauer
    - [x] Vorrechnung - Freiwillige oder nach System?
- [x] Besprechung von Blatt 0
    - 1. Vorrechnungen?
    - 2. Argument
    - 3. Vorrechnung? Beweis
        - gdw. Teil
        - Eindeutigkeit
- [x] Bisherige Themen aus VL
    - [ ] Ordnungsrelationen (auch Halbordnung genannt) vs. LO
    - [ ] »die« Struktur (N,v) mit (N,v) |= PA
    - [x] vollst. Ind (vs. schwache Induktion)
    - [ ] Rekursion
    - [x] Mengenkomplemente
    - [x] A2(b): wieder eine endliche Menge

## Nächste Woche ##

- Besprechung von ÜB1 inkl. Vorrechnen
- Stoff aus Kapitel 2

### TODOs (Studierende) ###

- Stoff aus Kapitel 1 erlernen
- Stoff aus Kapitel 2 durchlesen
- am ÜB1 weiter arbeiten
- Vorrechnen vorbereiten
