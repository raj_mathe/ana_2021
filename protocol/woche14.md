# Vorlesungswoche 14 (24.–30. Januar 2022) #

## Agenda ##

- [ ] Orga
  - Relevante Teile für Klausur?
  - Noten, Zulassung?
- [x] ÜB12
- [x] Fragen zur Klausur

## Nächste Woche ##

- zur Klausurvorbereitung
- ~~ÜB13~~ <— zugunsten der Klausur nicht.

### TODOs (Studierende) ###

- ggf. ÜB13 abgeben.
- Für Klausur vorbereiten:
  - eigene Zusammenfassung aufschreiben.
  - sicherstellen, dass alle relevanten Teile abgedeckt sind.
  - Aufgabensammlung/Probeklausur probieren.
