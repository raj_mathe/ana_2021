# Vorlesungswoche 4 (1.–7. November 2021) #

!!! Diese Woche sind wir im **Hörsaal 8** !!!

## Agenda ##

- [x] Orga
  - [x] ÜB1 zurückgeben (+ restliche ÜB0)
  - Anmerkung im Moodle:

        Es gilt immer (es sei denn, es wird etwas anderes gesagt),
        dass alle in Ihren Antworten gemachten Aussagen
            zu begründen bzw. herzuleiten sind.
        Aus der VL Bekanntes dürfen Sie mit "Zitat"
        (also etwa "Nach Vorlesung gilt...") verwenden.

  - [x] Takt bzgl. Besprechung der abgegebenen Blätter
- [x] Besprechung von Blatt 2
  - [x] A1, Vorrechnen?
  - [x] A2, Vorrechnen?
  - [x] A3, Vorrechnen? ---> nur Skizze
  - [x] A4, Vorrechnen? ---> nur Induktionsschritt
- [ ] Besprechung von Blatt 3 ---> nächste Woche?
  - [ ] A1. alternative Aufgabe (zwecks Stil, nicht Inhalt):
    - Zeige, dass 0·x = 0 für alle x ∈ K gilt.
  - [ ] A2. Alternativ mit F_5 (Skizze).
    - **Definition:** char(K) := min n ∈ ℕ, > 0 mit 1 + 1 + ... + 1 (n-Mal) = 0; sonst 0.
    - **Satz:** char(K) ∈ ℙ u {0}
    - **Definition:** Axiome für Körperanordnungen
            O1: Körper hat Kegelzerlegung
            O2: Kegel unter Addition und Multiplikation stabil
    - Beachte: positiv vs. **nicht negativ** !! (Und Abweichungen davon in versch. Gebieten.)
    - **Lemma:** Wenn char(K) ≠ 0, dann lässt sich K nicht mit Anordnung ausstatten.
  - [ ] A3. Beziehung zw. (a) und (b)?


## Nächste Woche ##

- ÜB3 vorrechnen?
- bisheriger Stoff

### TODOs (Studierende) ###

- Stoff aus Kapiteln 1–3 verinnerlichen
- am ÜB3 weiter arbeiten
- an das Vorrechnen für ÜB2 denken
