# Vorlesungswoche 10 (13.–19. Dezember 2021) #

Letzte Übung für 2021 vor den Weihnachtsferien.

## Agenda ##

- [x] ÜB 7
- [x] ÜB 9: Begriffserklärungen + „zum Nachdenken“ Hinweise zu A3, Z2.

## Nächste Wochen ##

- Weihnachten! 🌲 ☃️

### TODOs (Studierende) ###

- ÜB 9 bis 04.01. fertig schreiben.
