# Vorlesungswoche 1 (11.–17. Oktober 2021) #

Diese Woche wird Übungsblatt 0 veröffentlicht.

## Agenda ##

- [x] Organisatorisches
- [x] Eisbrecher
- [x] Aufgaben:
    - [x] Aussagenlogik
    - [x] Quantifizierte Aussagen
    - [x] Quantifizierte Aussagen über Relationen
    - [ ] Relation als Graph einer Funktion
        ```text
        Sei ƒ : ℝ ⟶ ]0, ∞[ die Funktion mit Vorschrift ƒ(x) = exp(-x).
        Sei R = { (x,y) ∈ ℝ ⨉ ]0, ∞[ | ƒ(x)=y }.
        Bestimmen Sie, welche der folgenden Aussagen gelten:

        i)   ƒ ist wohldefiniert
        ii)  ∀x∈X: ∃y∈X: R(x,y)
        iii) für kein x ∈ X existieren y₁, y₂ ∈ X so, dass
                y₁ ≠ y₂ und R(x,y₁) und R(x,y₂)
        iv)  ƒ ist injektiv
        v)   ƒ ist surjektiv
        vi)  ƒ ist bijektiv
        ```
    - [ ] Beweise:
        ```text
        Angenommen, R sei eine reflexive Relation über einer Grundmenge, X.
        Dann gilt ∀x∈X: ∃y∈X: R(x,y).
        ```

## Nächste Woche ##

- Besprechung vom Blatt 0.

### TODOs (Studierende) ###

- am ÜB 0 weiter arbeiten.
