# Vorlesungswoche 3 (25.–31. Oktober 2021) #

## Agenda ##

- [x] Orga
    - [x] Notenvergabe (4 x 4 pro Blatt)
    - [x] ÜB0 zurückgeben

<!-- ggf überspringen? -->
- [x] Besprechung von Blatt 1
    - [x] 1. Vorrechnen?
    - [x] 2. Vorrechnen?
    - [x] 3. Vorrechnen?
    - [ ] 4. Vorrechnen? --> siehe [notes/woche3.pdf](../notes/woche3.pdf)
    - [x] Z1. Vorrechnen?

- [x] Übungsblatt 2 diskutieren/Hinweise
    - [x] A1. Was ist zu zeigen? (Aufstellung der Behauptung.)
    - [x] A2. Was ist zu zeigen?
        - a) Induktionsbehauptung als Lemma.
        - b) (totale?) Ordnungsrelation:
          - Reflexivität
          - Transitivität
          - Antismymetrie
          - (total?)

        Was davon ist trivial? Was davon geht auf Ergebnisse in VL zurück? Was davon braucht (a)?

    - [x] A3. Was ist zu zeigen?
      - Welcher Teil ist trivial?
      - Welcher Teil ist nicht trivial?
        - Auf was kann man das Problem reduzieren? (Kann man hier Informationen aus anderen Aufgaben einsetzen?)
        - Welche Mittel kann mein einsetzen, um dies zu zeigen? (Wie stellt man hier eine passende Behauptung auf?)

    - [x] A4. Induktion ab n = 0!

## Nächste Woche ##

- am 4.11. im Hörsaal 8!
- Besprechung von ÜB2 inkl. Vorrechnen
- ggf. Stoff aus Kapitel 2 (Körper)

### TODOs (Studierende) ###

- Stoff aus Kapiteln 1–2 verinnerlichen
- am ÜB2 weiter arbeiten
- an das Vorrechnen für ÜB2 denken
