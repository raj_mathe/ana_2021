# Analysis I für Mathematiker, WiSe 2021-22 #

Diese Repository ist für die Übungsgruppe am Donnerstag um 15:15--16:45.

**HINWEIS:** In diesem Repository werden keine persönlichen Daten gespeichert.

In diesem Repository findet man:

- Protokolle der Übungsgruppe [hier](./protocol).
- Notizen [hier](./notes).
- Symbolverzeichnis unter [notes/glossar.md](./notes/glossar.md).
- Referenzen unter [notes/quellen.md](./notes/quellen.md).

## Klausur ##

**Bitte entnehmen Sie alle aktuellen Infos von Moodle.**

Zur Info:

- Klausurdatum nach letzter VL-Woche^.
</br>
(^ letzte VL-Woche = [31.1.--6.2.2022.] = [Woche 5, 2022])
- Probeklausur + Aufgabensammlung auf Moodle verfügbar.
